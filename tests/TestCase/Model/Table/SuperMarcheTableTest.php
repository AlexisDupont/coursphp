<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\SuperMarcheTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\SuperMarcheTable Test Case
 */
class SuperMarcheTableTest extends TestCase
{
    /**
     * Test subject
     *
     * @var \App\Model\Table\SuperMarcheTable
     */
    public $SuperMarche;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.SuperMarche'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::getTableLocator()->exists('SuperMarche') ? [] : ['className' => SuperMarcheTable::class];
        $this->SuperMarche = TableRegistry::getTableLocator()->get('SuperMarche', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->SuperMarche);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
