<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * SuperMarche Controller
 *
 * @property \App\Model\Table\SuperMarcheTable $SuperMarche
 *
 * @method \App\Model\Entity\SuperMarche[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class SuperMarcheController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $superMarche = $this->paginate($this->SuperMarche);

        $this->set(compact('superMarche'));
    }

    /**
     * View method
     *
     * @param string|null $id Super Marche id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $superMarche = $this->SuperMarche->get($id, [
            'contain' => []
        ]);

        $this->set('superMarche', $superMarche);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $superMarche = $this->SuperMarche->newEntity();
        if ($this->request->is('post')) {
            $superMarche = $this->SuperMarche->patchEntity($superMarche, $this->request->getData());
            if ($this->SuperMarche->save($superMarche)) {
                $this->Flash->success(__('The super marche has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The super marche could not be saved. Please, try again.'));
        }
        $this->set(compact('superMarche'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Super Marche id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $superMarche = $this->SuperMarche->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $superMarche = $this->SuperMarche->patchEntity($superMarche, $this->request->getData());
            if ($this->SuperMarche->save($superMarche)) {
                $this->Flash->success(__('The super marche has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The super marche could not be saved. Please, try again.'));
        }
        $this->set(compact('superMarche'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Super Marche id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $superMarche = $this->SuperMarche->get($id);
        if ($this->SuperMarche->delete($superMarche)) {
            $this->Flash->success(__('The super marche has been deleted.'));
        } else {
            $this->Flash->error(__('The super marche could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
