<?php
namespace App\Controller;

use App\Controller\AppController;

/**
 * Dresseur Controller
 *
 * @property \App\Model\Table\DresseurTable $Dresseur
 *
 * @method \App\Model\Entity\Dresseur[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class DresseurController extends AppController
{
    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $dresseur = $this->paginate($this->Dresseur);

        $this->set(compact('dresseur'));
    }

    /**
     * View method
     *
     * @param string|null $id Dresseur id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $dresseur = $this->Dresseur->get($id, [
            'contain' => []
        ]);

        $this->set('dresseur', $dresseur);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $dresseur = $this->Dresseur->newEntity();
        if ($this->request->is('post')) {
            $dresseur = $this->Dresseur->patchEntity($dresseur, $this->request->getData());
            if ($this->Dresseur->save($dresseur)) {
                $this->Flash->success(__('The dresseur has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dresseur could not be saved. Please, try again.'));
        }
        $this->set(compact('dresseur'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Dresseur id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        $dresseur = $this->Dresseur->get($id, [
            'contain' => []
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $dresseur = $this->Dresseur->patchEntity($dresseur, $this->request->getData());
            if ($this->Dresseur->save($dresseur)) {
                $this->Flash->success(__('The dresseur has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The dresseur could not be saved. Please, try again.'));
        }
        $this->set(compact('dresseur'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Dresseur id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        $this->request->allowMethod(['post', 'delete']);
        $dresseur = $this->Dresseur->get($id);
        if ($this->Dresseur->delete($dresseur)) {
            $this->Flash->success(__('The dresseur has been deleted.'));
        } else {
            $this->Flash->error(__('The dresseur could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
