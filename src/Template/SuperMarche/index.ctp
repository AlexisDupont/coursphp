<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SuperMarche[]|\Cake\Collection\CollectionInterface $superMarche
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('New Super Marche'), ['action' => 'add']) ?></li>
    </ul>
</nav>
<div class="superMarche index large-9 medium-8 columns content">
    <h3><?= __('Super Marche') ?></h3>
    <table cellpadding="0" cellspacing="0">
        <thead>
            <tr>
                <th scope="col"><?= $this->Paginator->sort('id') ?></th>
                <th scope="col"><?= $this->Paginator->sort('prix') ?></th>
                <th scope="col"><?= $this->Paginator->sort('created') ?></th>
                <th scope="col"><?= $this->Paginator->sort('modified') ?></th>
                <th scope="col" class="actions"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($superMarche as $superMarche): ?>
            <tr>
                <td><?= $this->Number->format($superMarche->id) ?></td>
                <td><?= $this->Number->format($superMarche->prix) ?></td>
                <td><?= h($superMarche->created) ?></td>
                <td><?= h($superMarche->modified) ?></td>
                <td class="actions">
                    <?= $this->Html->link(__('View'), ['action' => 'view', $superMarche->id]) ?>
                    <?= $this->Html->link(__('Edit'), ['action' => 'edit', $superMarche->id]) ?>
                    <?= $this->Form->postLink(__('Delete'), ['action' => 'delete', $superMarche->id], ['confirm' => __('Are you sure you want to delete # {0}?', $superMarche->id)]) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>
    <div class="paginator">
        <ul class="pagination">
            <?= $this->Paginator->first('<< ' . __('first')) ?>
            <?= $this->Paginator->prev('< ' . __('previous')) ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next(__('next') . ' >') ?>
            <?= $this->Paginator->last(__('last') . ' >>') ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>
</div>
