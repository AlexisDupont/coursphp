<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SuperMarche $superMarche
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Html->link(__('Edit Super Marche'), ['action' => 'edit', $superMarche->id]) ?> </li>
        <li><?= $this->Form->postLink(__('Delete Super Marche'), ['action' => 'delete', $superMarche->id], ['confirm' => __('Are you sure you want to delete # {0}?', $superMarche->id)]) ?> </li>
        <li><?= $this->Html->link(__('List Super Marche'), ['action' => 'index']) ?> </li>
        <li><?= $this->Html->link(__('New Super Marche'), ['action' => 'add']) ?> </li>
    </ul>
</nav>
<div class="superMarche view large-9 medium-8 columns content">
    <h3><?= h($superMarche->id) ?></h3>
    <table class="vertical-table">
        <tr>
            <th scope="row"><?= __('Id') ?></th>
            <td><?= $this->Number->format($superMarche->id) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Prix') ?></th>
            <td><?= $this->Number->format($superMarche->prix) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Created') ?></th>
            <td><?= h($superMarche->created) ?></td>
        </tr>
        <tr>
            <th scope="row"><?= __('Modified') ?></th>
            <td><?= h($superMarche->modified) ?></td>
        </tr>
    </table>
    <div class="row">
        <h4><?= __('Produit') ?></h4>
        <?= $this->Text->autoParagraph(h($superMarche->produit)); ?>
    </div>
</div>
