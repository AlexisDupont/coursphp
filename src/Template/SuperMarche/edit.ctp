<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\SuperMarche $superMarche
 */
?>
<nav class="large-3 medium-4 columns" id="actions-sidebar">
    <ul class="side-nav">
        <li class="heading"><?= __('Actions') ?></li>
        <li><?= $this->Form->postLink(
                __('Delete'),
                ['action' => 'delete', $superMarche->id],
                ['confirm' => __('Are you sure you want to delete # {0}?', $superMarche->id)]
            )
        ?></li>
        <li><?= $this->Html->link(__('List Super Marche'), ['action' => 'index']) ?></li>
    </ul>
</nav>
<div class="superMarche form large-9 medium-8 columns content">
    <?= $this->Form->create($superMarche) ?>
    <fieldset>
        <legend><?= __('Edit Super Marche') ?></legend>
        <?php
            echo $this->Form->control('produit');
            echo $this->Form->control('prix');
        ?>
    </fieldset>
    <?= $this->Form->button(__('Submit')) ?>
    <?= $this->Form->end() ?>
</div>
